"use strict";
const webpack = require("webpack");
const path = require("path");
const fs = require("fs");

const pkg = require("./package.json");

const ENVIRONMENT = process.env.NODE_ENV || "development";
const IS_PRODUCTION = ENVIRONMENT === "production";

const OUTPUT_PATH = path.join(__dirname, "dist");

const DEFINITIONS = new webpack.DefinePlugin({
  TITLE: JSON.stringify(pkg.title),
  VERSION: JSON.stringify(pkg.version),
  PKG_NAME: JSON.stringify(pkg.name),
  DESCRIPTION: JSON.stringify(pkg.description),
  ENVIRONMENT: JSON.stringify(ENVIRONMENT),
  IS_PRODUCTION: JSON.stringify(IS_PRODUCTION),
  BUILD_DATE: JSON.stringify(new Date()),
  //Used by ReactJS to turn optimization on/off
  "process.env": {
    NODE_ENV: JSON.stringify(ENVIRONMENT)
  }
});

const LOADER_OPTIONS = new webpack.LoaderOptionsPlugin({
  minimize: IS_PRODUCTION,
  debug: !IS_PRODUCTION,
  options: {
    context: __dirname
  }
});

const UglifyJSPlugin = require("uglifyjs-webpack-plugin");

let plugins = [DEFINITIONS, LOADER_OPTIONS];

const entries = {
  index: [path.join(__dirname, "src", "index.js")]
};

const JS_LOADERS = ["babel-loader"];
if (ENVIRONMENT === "production") {
  JS_LOADERS.push("strip-loader?strip[]=console.debug,strip[]=console.log");
  plugins.push(
    new UglifyJSPlugin({
      uglifyOptions: { ecma: 8 },
      sourceMap: true
    })
  );
}

let webpackConfig = {
  context: path.join(__dirname, "src"),
  entry: entries,
  devServer: {
    host: "0.0.0.0" //Any network interface
  },
  output: {
    path: OUTPUT_PATH,
    library: pkg.name,
    libraryTarget: "umd",
    filename: IS_PRODUCTION ? "[name].min.js" : "[name].js"
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules)/,
        use: JS_LOADERS
      }
    ]
  },
  resolve: {
    extensions: [
      ".js", // automatically in webpack 2
      ".jsx",
      ".json" // automatically in webpack 2
    ],
    modules: ["node_modules", path.resolve(__dirname, "./node_modules"), "src"]
  },
  externals: {
    "is-pojo": "is-pojo",
    localforage: "localforage",
    "lodash.throttle": "lodash.throttle",
    react: "react",
    "react-dom": "react-dom",
    "react-redux": "react-redux",
    redux: "redux",
    "redux-persist": "redux-persist",
    "redux-reduce-with": "redux-reduce-with",
    "redux-thunk": "redux-thunk",
    reselect: "reselect"
  },
  plugins: plugins,
  devtool: "source-map"
};

module.exports = webpackConfig;
