# Device Orientation (Compass)

This module provides compass redux state updates through `deviceorientation` event (W3C API).  
It provides a dataSource with state definition and action creators, and a dataProvider (withCompass) for connecting react components.

This module is part of the [I-REACT Open Core](https://mobilesolutionsismb.bitbucket.io/i-react-open-core/).

## Acknowledgment

This work was partially funded by the European Commission through the [I-REACT project](http://www.i-react.eu/) (H2020-DRS-1-2015), grant agreement n.700256.

## Action Creators

`changeHeadingFrequency(frequency: int)` change frequency (ms)
`watchHeadingStart()` - start updating
`watchHeadingStop()` - stop updating

## Data Providers

`withCompass` - provides heading as heading: `{value, timestamp}`