import { reduceWith } from "redux-reduce-with";
import DefaultState from "./DefaultState";
import {
  HEADING_UPDATED,
  WATCHING_HEADING_START,
  WATCHING_HEADING_STOP,
  SETTINGS_CHANGED
} from "./Actions";

const mutators = {
  [HEADING_UPDATED]: {
    heading: action => action.heading, //sensor value
    normalizedHeading: action => action.normalizedHeading, //normalized for compass display
    timestamp: action => action.timestamp,
    isAbsolute: action => action.isAbsolute,
    headingAccuracy: action => action.accuracy,
    headingError: action => action.error
  },
  [WATCHING_HEADING_START]: {
    watching: true
  },
  [WATCHING_HEADING_STOP]: {
    watching: false
  },
  [SETTINGS_CHANGED]: {
    settings: action => action.settings
  }
};

export default reduceWith(mutators, DefaultState);
