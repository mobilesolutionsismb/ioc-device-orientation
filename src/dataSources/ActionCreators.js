import DefaultState from './DefaultState';
import {
    HEADING_UPDATED,
    /*     HEADING_ERROR, */
    WATCHING_HEADING_START,
    WATCHING_HEADING_STOP,
    SETTINGS_CHANGED/* ,
    UPDATE_CALIBRATION_FLAG */
} from './Actions';
import throttle from 'lodash.throttle';
import { normalizeRotation } from '../normalizeRotation';

let listener = null; //device orientation event listener

// deviceorientationabsolute is supported only by Chrome Mobile and Android WebView
// It makes sense only on Android
// See also https://timvolodine.github.io/deviceorientation-test/
const DEVICE_EVENT_NAME = 'ondeviceorientationabsolute' in window && navigator.userAgent.indexOf('Android') > -1 ?
    'deviceorientationabsolute' : 'deviceorientation';

// See https://blog.phonegap.com/migrating-from-the-cordova-device-orientation-plugin-8442b869e6cc
function _getHandler(updateCb, getState) {
    return function handler(event) {
        const IS_IOS_POSITIONING = typeof event.webkitCompassHeading !== 'undefined';
        const isAbsolute = IS_IOS_POSITIONING || event.absolute === true;
        // const value = IS_IOS_POSITIONING ? Math.round(event.webkitCompassHeading) : Math.round(360 - event.alpha);
        const value = IS_IOS_POSITIONING ? Math.round(event.webkitCompassHeading) : Math.round(event.alpha);

        console.debug(DEVICE_EVENT_NAME, event, value);
        if (IS_IOS_POSITIONING && event.webkitCompassAccuracy < 0) {
            // Device needs calibration
            updateCb(-1, -1, Date.now(), isAbsolute, event.webkitCompassAccuracy, new Error('Device needs calibration'));
        }
        else if (IS_IOS_POSITIONING && event.webkitCompassHeading < 0) {
            // Error: see https://developer.apple.com/documentation/webkitjs/deviceorientationevent/1804777-webkitcompassheading
            updateCb(-1, -1, Date.now(), isAbsolute, event.webkitCompassAccuracy, new Error('Invalid direction'));
        }
        else {
            const prevValue = getState().deviceOrientation.normalizedHeading;
            const normValue = normalizeRotation(value, prevValue);
            updateCb(value, normValue, Date.now(), isAbsolute, event.webkitCompassAccuracy || null, null);
        }
    };
}

function _getUpdateCb(dispatch) {
    return (heading, normalizedHeading, timestamp, isAbsolute, accuracy, error) => dispatch({
        type: HEADING_UPDATED,
        heading,
        normalizedHeading,
        timestamp,
        isAbsolute,
        accuracy,
        error
    });
}

function _clearListener() {
    if (listener !== null) {
        listener.cancel();
        window.removeEventListener(DEVICE_EVENT_NAME, listener, true);
        listener = null;
    }
}

function _addListener(frequency, updateCb, getState) {
    if (listener === null) {
        const handler = _getHandler(updateCb, getState);
        listener = throttle(handler, frequency);
        window.addEventListener(DEVICE_EVENT_NAME, listener, true);
    }
}

export function changeHeadingFrequency(frequency) {
    return (dispatch, getState) => {
        if (typeof frequency === 'number' && frequency >= 100) {
            _clearListener();
            const updateCb = _getUpdateCb.call(this, dispatch);
            _addListener(frequency, updateCb, getState);
            dispatch({
                type: SETTINGS_CHANGED,
                settings: {
                    frequency
                }
            });
        }
    };
}


export function watchHeadingStart() {
    return (dispatch, getState) => {
        if (listener === null) {
            const frequency = getState().deviceOrientation.settings.frequency;
            const updateCb = _getUpdateCb.call(this, dispatch);
            _addListener(frequency, updateCb, getState);
            dispatch({
                type: WATCHING_HEADING_START
            });
        }
    };
}


export function watchHeadingStop() {
    return (dispatch, getState) => {
        if (listener !== null) {
            _clearListener();
            dispatch({
                type: WATCHING_HEADING_STOP
            });
        }
    };
}