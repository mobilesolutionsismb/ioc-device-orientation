const STATE = {
    watching: false,
    headingError: null,
    heading: 0,
    normalizedHeading: 0,
    isAbsolute: false, // see https://developer.mozilla.org/en-US/docs/Web/API/DeviceOrientationEvent/absolute
    // DEPRECATED: https://github.com/apache/cordova-plugin-device-orientation#deprecation-notice
    // magneticHeading: 0, // The heading in degrees from 0-359.99 at a single moment in time. (Number)
    // trueHeading: 0, // The heading relative to the geographic North Pole in degrees 0-359.99 at a single moment in time. A negative value indicates that the true heading can't be determined. (Number)
    headingAccuracy: null, //The deviation in degrees between the reported heading and the true heading. (Number) - can be not available (null)
    timestamp: 0, // The time at which this heading was determined. (DOMTimeStamp)
    settings: {
        frequency: 300 //Default value - not using filter since it's supported by iOS only
    }
};

export default STATE;