import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const headingErrorSelector = state => state.deviceOrientation.headingError;
const headingSelector = state => {
    const { heading, normalizedHeading, timestamp, isAbsolute, headingAccuracy } = state.deviceOrientation;
    return {
        value: heading,
        normalizedValue: normalizedHeading,
        timestamp,
        isAbsolute,
        accuracy: headingAccuracy
    };
};
const headingWatchingSelector = state => state.deviceOrientation.watching;
const headingUpdateFrequencySelector = state => state.deviceOrientation.settings.frequency;

const select = createStructuredSelector({
    headingError: headingErrorSelector,
    headingWatching: headingWatchingSelector,
    heading: headingSelector,
    headingUpdateFrequency: headingUpdateFrequencySelector
});

export default connect(select, ActionCreators);