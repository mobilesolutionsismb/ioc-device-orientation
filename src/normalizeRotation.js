/**
 * Normalize rotation shifting
 * @param {Number} nextRot 
 * @param {Number} prevRot 
 */
export function normalizeRotation(nextRot, prevRot = 0) {
    let rot = prevRot;
    let aR = rot % 360;
    if (aR < 0) {
        aR += 360;
    }
    if (aR < 180 && (nextRot - aR > 180)) {
        rot -= 360;
    }
    if (aR >= 180 && (nextRot <= (aR - 180))) {
        rot += 360;
    }
    rot += (nextRot - aR);
    return rot;
}