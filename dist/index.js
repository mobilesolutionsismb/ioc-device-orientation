(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("lodash.throttle"), require("react-redux"), require("reselect"), require("redux-reduce-with"));
	else if(typeof define === 'function' && define.amd)
		define(["lodash.throttle", "react-redux", "reselect", "redux-reduce-with"], factory);
	else if(typeof exports === 'object')
		exports["ioc-device-orientation"] = factory(require("lodash.throttle"), require("react-redux"), require("reselect"), require("redux-reduce-with"));
	else
		root["ioc-device-orientation"] = factory(root["lodash.throttle"], root["react-redux"], root["reselect"], root["redux-reduce-with"]);
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_5__, __WEBPACK_EXTERNAL_MODULE_8__, __WEBPACK_EXTERNAL_MODULE_9__, __WEBPACK_EXTERNAL_MODULE_11__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.changeHeadingFrequency = changeHeadingFrequency;
exports.watchHeadingStart = watchHeadingStart;
exports.watchHeadingStop = watchHeadingStop;

var _DefaultState = __webpack_require__(1);

var _DefaultState2 = _interopRequireDefault(_DefaultState);

var _Actions = __webpack_require__(2);

var _lodash = __webpack_require__(5);

var _lodash2 = _interopRequireDefault(_lodash);

var _normalizeRotation = __webpack_require__(6);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var listener = null; //device orientation event listener

// deviceorientationabsolute is supported only by Chrome Mobile and Android WebView
// It makes sense only on Android
// See also https://timvolodine.github.io/deviceorientation-test/
var DEVICE_EVENT_NAME = 'ondeviceorientationabsolute' in window && navigator.userAgent.indexOf('Android') > -1 ? 'deviceorientationabsolute' : 'deviceorientation';

// See https://blog.phonegap.com/migrating-from-the-cordova-device-orientation-plugin-8442b869e6cc
function _getHandler(updateCb, getState) {
    return function handler(event) {
        var IS_IOS_POSITIONING = typeof event.webkitCompassHeading !== 'undefined';
        var isAbsolute = IS_IOS_POSITIONING || event.absolute === true;
        // const value = IS_IOS_POSITIONING ? Math.round(event.webkitCompassHeading) : Math.round(360 - event.alpha);
        var value = IS_IOS_POSITIONING ? Math.round(event.webkitCompassHeading) : Math.round(event.alpha);

        console.debug(DEVICE_EVENT_NAME, event, value);
        if (IS_IOS_POSITIONING && event.webkitCompassAccuracy < 0) {
            // Device needs calibration
            updateCb(-1, -1, Date.now(), isAbsolute, event.webkitCompassAccuracy, new Error('Device needs calibration'));
        } else if (IS_IOS_POSITIONING && event.webkitCompassHeading < 0) {
            // Error: see https://developer.apple.com/documentation/webkitjs/deviceorientationevent/1804777-webkitcompassheading
            updateCb(-1, -1, Date.now(), isAbsolute, event.webkitCompassAccuracy, new Error('Invalid direction'));
        } else {
            var prevValue = getState().deviceOrientation.normalizedHeading;
            var normValue = (0, _normalizeRotation.normalizeRotation)(value, prevValue);
            updateCb(value, normValue, Date.now(), isAbsolute, event.webkitCompassAccuracy || null, null);
        }
    };
}

function _getUpdateCb(dispatch) {
    return function (heading, normalizedHeading, timestamp, isAbsolute, accuracy, error) {
        return dispatch({
            type: _Actions.HEADING_UPDATED,
            heading: heading,
            normalizedHeading: normalizedHeading,
            timestamp: timestamp,
            isAbsolute: isAbsolute,
            accuracy: accuracy,
            error: error
        });
    };
}

function _clearListener() {
    if (listener !== null) {
        listener.cancel();
        window.removeEventListener(DEVICE_EVENT_NAME, listener, true);
        listener = null;
    }
}

function _addListener(frequency, updateCb, getState) {
    if (listener === null) {
        var handler = _getHandler(updateCb, getState);
        listener = (0, _lodash2.default)(handler, frequency);
        window.addEventListener(DEVICE_EVENT_NAME, listener, true);
    }
}

function changeHeadingFrequency(frequency) {
    var _this = this;

    return function (dispatch, getState) {
        if (typeof frequency === 'number' && frequency >= 100) {
            _clearListener();
            var updateCb = _getUpdateCb.call(_this, dispatch);
            _addListener(frequency, updateCb, getState);
            dispatch({
                type: _Actions.SETTINGS_CHANGED,
                settings: {
                    frequency: frequency
                }
            });
        }
    };
}

function watchHeadingStart() {
    var _this2 = this;

    return function (dispatch, getState) {
        if (listener === null) {
            var frequency = getState().deviceOrientation.settings.frequency;
            var updateCb = _getUpdateCb.call(_this2, dispatch);
            _addListener(frequency, updateCb, getState);
            dispatch({
                type: _Actions.WATCHING_HEADING_START
            });
        }
    };
}

function watchHeadingStop() {
    return function (dispatch, getState) {
        if (listener !== null) {
            _clearListener();
            dispatch({
                type: _Actions.WATCHING_HEADING_STOP
            });
        }
    };
}

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var STATE = {
    watching: false,
    headingError: null,
    heading: 0,
    normalizedHeading: 0,
    isAbsolute: false, // see https://developer.mozilla.org/en-US/docs/Web/API/DeviceOrientationEvent/absolute
    // DEPRECATED: https://github.com/apache/cordova-plugin-device-orientation#deprecation-notice
    // magneticHeading: 0, // The heading in degrees from 0-359.99 at a single moment in time. (Number)
    // trueHeading: 0, // The heading relative to the geographic North Pole in degrees 0-359.99 at a single moment in time. A negative value indicates that the true heading can't be determined. (Number)
    headingAccuracy: null, //The deviation in degrees between the reported heading and the true heading. (Number) - can be not available (null)
    timestamp: 0, // The time at which this heading was determined. (DOMTimeStamp)
    settings: {
        frequency: 300 //Default value - not using filter since it's supported by iOS only
    }
};

exports.default = STATE;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var NS = 'DEVICE_ORIENTATION';

var HEADING_UPDATED = exports.HEADING_UPDATED = NS + '@HEADING_UPDATED';
// export const HEADING_ERROR = `${NS}@HEADING_ERROR`;
var WATCHING_HEADING_START = exports.WATCHING_HEADING_START = NS + '@WATCHING_HEADING_START';
var WATCHING_HEADING_STOP = exports.WATCHING_HEADING_STOP = NS + '@WATCHING_HEADING_STOP';
var SETTINGS_CHANGED = exports.SETTINGS_CHANGED = NS + '@SETTINGS_CHANGED';

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(4);


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Reducer = exports.withCompass = undefined;

var _ActionCreators = __webpack_require__(0);

Object.keys(_ActionCreators).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _ActionCreators[key];
    }
  });
});

var _withCompass = __webpack_require__(7);

Object.defineProperty(exports, 'withCompass', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_withCompass).default;
  }
});

var _Reducer = __webpack_require__(10);

var _Reducer2 = _interopRequireDefault(_Reducer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.Reducer = _Reducer2.default;

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_5__;

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.normalizeRotation = normalizeRotation;
/**
 * Normalize rotation shifting
 * @param {Number} nextRot 
 * @param {Number} prevRot 
 */
function normalizeRotation(nextRot) {
    var prevRot = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

    var rot = prevRot;
    var aR = rot % 360;
    if (aR < 0) {
        aR += 360;
    }
    if (aR < 180 && nextRot - aR > 180) {
        rot -= 360;
    }
    if (aR >= 180 && nextRot <= aR - 180) {
        rot += 360;
    }
    rot += nextRot - aR;
    return rot;
}

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _reactRedux = __webpack_require__(8);

var _reselect = __webpack_require__(9);

var _ActionCreators = __webpack_require__(0);

var ActionCreators = _interopRequireWildcard(_ActionCreators);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var headingErrorSelector = function headingErrorSelector(state) {
    return state.deviceOrientation.headingError;
};
var headingSelector = function headingSelector(state) {
    var _state$deviceOrientat = state.deviceOrientation,
        heading = _state$deviceOrientat.heading,
        normalizedHeading = _state$deviceOrientat.normalizedHeading,
        timestamp = _state$deviceOrientat.timestamp,
        isAbsolute = _state$deviceOrientat.isAbsolute,
        headingAccuracy = _state$deviceOrientat.headingAccuracy;

    return {
        value: heading,
        normalizedValue: normalizedHeading,
        timestamp: timestamp,
        isAbsolute: isAbsolute,
        accuracy: headingAccuracy
    };
};
var headingWatchingSelector = function headingWatchingSelector(state) {
    return state.deviceOrientation.watching;
};
var headingUpdateFrequencySelector = function headingUpdateFrequencySelector(state) {
    return state.deviceOrientation.settings.frequency;
};

var select = (0, _reselect.createStructuredSelector)({
    headingError: headingErrorSelector,
    headingWatching: headingWatchingSelector,
    heading: headingSelector,
    headingUpdateFrequency: headingUpdateFrequencySelector
});

exports.default = (0, _reactRedux.connect)(select, ActionCreators);

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_8__;

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_9__;

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mutators;

var _reduxReduceWith = __webpack_require__(11);

var _DefaultState = __webpack_require__(1);

var _DefaultState2 = _interopRequireDefault(_DefaultState);

var _Actions = __webpack_require__(2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var mutators = (_mutators = {}, _defineProperty(_mutators, _Actions.HEADING_UPDATED, {
  heading: function heading(action) {
    return action.heading;
  }, //sensor value
  normalizedHeading: function normalizedHeading(action) {
    return action.normalizedHeading;
  }, //normalized for compass display
  timestamp: function timestamp(action) {
    return action.timestamp;
  },
  isAbsolute: function isAbsolute(action) {
    return action.isAbsolute;
  },
  headingAccuracy: function headingAccuracy(action) {
    return action.accuracy;
  },
  headingError: function headingError(action) {
    return action.error;
  }
}), _defineProperty(_mutators, _Actions.WATCHING_HEADING_START, {
  watching: true
}), _defineProperty(_mutators, _Actions.WATCHING_HEADING_STOP, {
  watching: false
}), _defineProperty(_mutators, _Actions.SETTINGS_CHANGED, {
  settings: function settings(action) {
    return action.settings;
  }
}), _mutators);

exports.default = (0, _reduxReduceWith.reduceWith)(mutators, _DefaultState2.default);

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_11__;

/***/ })
/******/ ]);
});
//# sourceMappingURL=index.js.map